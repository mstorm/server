# 서버 설정

## 하드웨어

서버실에 서버를 설치하고 모니터, 키보드, 랜선, 전원을 연결합니다. 전원을 연결할 경우 잠시 후 자동으로 서버가 부팅되기 시작합니다.

부팅은 약 2분 ~ 5분 정도 소요 됩니다.

## 로그인

`root` 아이디와 미리 알려준 비밀번호로 로그인을 합니다.

## 네트워크 설정

`/etc/network/interfaces` 파일을 수정합니다.

파일 수정을 위해 다음 명령어를 실행 합니다.
```sh
vim /etc/network/interfaces
```

address, gateway, netmask 값을 적절하게 수정합니다.

```
auto eno1
iface eno1 inet static
address <IP 주소>
netmask <Netmask>
gateway <Gateway IP 주소>
```

`:wq` 명령어를 이용해 저장 후 에디터(VIM)를 종료 합니다.

## 재시작

`reboot` 명령어를 이용하여 시스템을 재시작 합니다.

모니터와 키보드는 네트워크 설정을 위해 필요했던 것으로 제거해도 됩니다.

----

[VIM 사용법](https://opentutorials.org/module/522/4559)
